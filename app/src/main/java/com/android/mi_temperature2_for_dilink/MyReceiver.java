package com.android.mi_temperature2_for_dilink;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.preference.PreferenceManager;

import com.socks.library.KLog;

public class MyReceiver extends BroadcastReceiver {

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        KLog.e("接收到开机广播啦");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean autoBootFloating = preferences.getBoolean(Constants.KEY_AUTO_BOOT_FLOATING, false);
        if (autoBootFloating) {
            Intent in = new Intent(context, MyService.class);
            in.putExtra("isBackground", true);
            AutoBootHelper.startForegroundService(context, in);
        }
    }
}