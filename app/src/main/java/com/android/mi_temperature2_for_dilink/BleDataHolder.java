package com.android.mi_temperature2_for_dilink;

import java.text.DecimalFormat;

public class BleDataHolder {
    private String lastMac;
    private String temperature = "00.00";
    private String humidity = "00";
    private String voltage = "0.000";
    private String leftPadding = "0";
    private String topPadding = "0";
    private boolean autoBoot = false;
    private String shuShiDu = "(?)";

    public String getLastMac() {
        return lastMac;
    }

    public void setLastMac(String lastMac) {
        this.lastMac = lastMac;
    }

    public String getTemperature() {
        return temperature;
    }

    DecimalFormat format = new DecimalFormat("00.00");
    DecimalFormat format1 = new DecimalFormat("0.000");

    public void setTemperature(String temperature) {
        int anInt = Integer.parseInt(temperature);
        float v = anInt * 1.0f / 100;
        this.temperature = format.format(v);
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        int anInt = Integer.parseInt(voltage);
        float v = anInt * 1.0f / 1000;
        this.voltage = format1.format(v);
    }

    public String getLeftPadding() {
        return leftPadding;
    }

    public void setLeftPadding(String leftPadding) {
        this.leftPadding = leftPadding;
    }

    public String getTopPadding() {
        return topPadding;
    }

    public void setTopPadding(String topPadding) {
        this.topPadding = topPadding;
    }

    public boolean isAutoBoot() {
        return autoBoot;
    }

    public void setAutoBoot(boolean autoBoot) {
        this.autoBoot = autoBoot;
    }

    private static String getShuShiDu(float temperature, float humidity) {
        if (temperature >= 19 && temperature <= 27) {
            if (humidity >= 20 && humidity <= 85) {
                return "(^_^)";
            }
        }
        return "(-ʌ-)";
    }

    public String getShuShiDu() {
        return shuShiDu;
    }

    public void setShuShiDu(int temperature, int humidity) {
        int anInt = Integer.parseInt(temperature + "");
        float vTemperature = anInt * 1.0f / 100;

        int vHumidity = humidity;

        String shuShiDu1 = getShuShiDu(vTemperature, vHumidity);
        this.shuShiDu = shuShiDu1;
    }
}
