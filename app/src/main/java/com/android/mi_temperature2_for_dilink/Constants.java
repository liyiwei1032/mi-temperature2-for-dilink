package com.android.mi_temperature2_for_dilink;

public class Constants {
    public static final String KEY_LAST_DEVICE_ADDRESS = "key_last_device_address";
    public static final String KEY_PADDING_LEFT = "key_padding_left";
    public static final String KEY_PADDING_TOP = "key_padding_top";
    public static final String KEY_AUTO_BOOT_FLOATING = "key_auto_boot_floating";
}
