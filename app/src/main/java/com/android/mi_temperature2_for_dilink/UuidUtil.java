package com.android.mi_temperature2_for_dilink;

public class UuidUtil {
    public static final String uuid10 = "00001800-0000-1000-8000-00805f9b34fb";
    public static final String uuid11 = "00002a00-0000-1000-8000-00805f9b34fb";
    public static final String uuid12 = "00002a01-0000-1000-8000-00805f9b34fb";
    public static final String uuid13 = "00002a04-0000-1000-8000-00805f9b34fb";


    public static final String uuid20 = "00001801-0000-1000-8000-00805f9b34fb";
    public static final String uuid21 = "00002a05-0000-1000-8000-00805f9b34fb";

    public static final String uuid30 = "";
    public static final String uuid31 = "";
    public static final String uuid32 = "";
    public static final String uuid33 = "";
    public static final String uuid34 = "";
    public static final String uuid35 = "";
    public static final String uuid36 = "";

    public static final String MAIN_DATA_SRV = "ebe0ccb0-7a0a-4b0c-8a1a-6ff2997da3a6";
    public static final String MAIN_DATA_CH = "ebe0ccc1-7a0a-4b0c-8a1a-6ff2997da3a6";

}
